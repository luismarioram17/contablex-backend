echo "Running startscript.sh"

npm install -g tsconfig-paths
npm install -g ts-node

echo "RUNNING migrations"
ts-node -r npm run typeorm migration:run

echo "RUNNING seeds"
SEEDING=true npm run typeorm migration:run