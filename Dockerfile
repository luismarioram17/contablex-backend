FROM node:latest As development

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=development

RUN npm install -g rimraf

COPY . .

RUN npm run build

FROM node:latest as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --omit=development

COPY . .

RUN chmod u+rwx /usr/src/app/scripts/startscript.sh

COPY --from=development /usr/src/app/dist ./dist

CMD ["node", "dist/main"]