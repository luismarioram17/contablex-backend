import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { join } from 'path';
import { DataSource, DataSourceOptions } from 'typeorm';

const databaseVars = process.env.DATABASE_URL
  ? {
      url: process.env.DATABASE_URL,
      ssl: {
        rejectUnauthorized: false,
      },
    }
  : {
      host: process.env.POSTGRES_HOST,
      port: 5432,
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
    };

let environment = 'migrations';

if (process.env.PRODUCTION) {
  environment = 'productionSeeds';
}

if (process.env.SEEDING) {
  environment = 'seeds';
}

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  ...databaseVars,
  migrationsTableName: environment,
  namingStrategy: new SnakeNamingStrategy(),
  entities: [join(__dirname, '..', 'data-source/entities/*.entity{.ts,.js}')],
  migrations: [join(__dirname, '..', `data-source/${environment}/*{.ts,.js}`)],
};

export default new DataSource(typeOrmConfig as DataSourceOptions);
