export enum AccountType {
  LIABILITIES = 'LIABILITIES',
  ASSETS = 'ASSETS',
  INCOME = 'INCOME',
  EXPENSES = 'EXPENSES',
}

export const types = [
  { label: 'Liabilities', value: AccountType.LIABILITIES },
  { label: 'Assets', value: AccountType.ASSETS },
  { label: 'Income', value: AccountType.INCOME },
  { label: 'Expenses', value: AccountType.EXPENSES },
];
