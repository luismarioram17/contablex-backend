import { AccountType } from 'src/config/enums';
import { In, MigrationInterface, QueryRunner } from 'typeorm';
import { Account } from 'src/data-source/entities';

export class AddDefaultAccounts1667276979298 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const accountRepository = await queryRunner.manager.getRepository(Account);

    await accountRepository.save([
      {
        label: 'CYB',
        name: 'Cajas y Bancos',
        description: 'Cuentas de bancos y cajas de la empresa',
        type: AccountType.ASSETS,
        current: true,
      },
      {
        label: 'CXC',
        name: 'Cuentas por cobrar',
        description: 'Cuentas que eventualmente serán cobradas',
        type: AccountType.ASSETS,
        current: true,
      },
      {
        label: 'INV',
        name: 'Inventarios',
        description: 'Inventarios de la empresa',
        type: AccountType.ASSETS,
        current: true,
      },
      {
        label: 'MTP',
        name: 'Materias primas',
        description: 'Materias primas para produccion',
        type: AccountType.ASSETS,
        current: true,
      },
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const accountRepository = await queryRunner.manager.getRepository(Account);

    await accountRepository.delete({
      label: In(['CYB', 'CXC', 'INV', 'MTP']),
    });
  }
}
