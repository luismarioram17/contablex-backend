import { MigrationInterface, QueryRunner } from 'typeorm';
import { hash } from 'bcrypt';

import { User } from 'src/data-source/entities';

export class AddAdminUser1667266307675 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const userRepository = queryRunner.manager.getRepository(User);

    await userRepository.save({
      email: 'admin@email.com',
      username: 'admin',
      firstName: 'admin',
      lastName: '',
      password: await hash('admin', 10),
    });
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const userRepository = queryRunner.manager.getRepository(User);
    userRepository.delete({
      email: 'admin@email.com',
    });
  }
}
