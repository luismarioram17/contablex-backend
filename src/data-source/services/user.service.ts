import { Injectable, NotFoundException } from '@nestjs/common';
import { hash } from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';

import { User } from 'src/data-source/entities';
import { UserRepository } from 'src/data-source/repositories';
import { CreateUserDto, UpdateUserDto } from 'src/data-source/dtos';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: UserRepository,
  ) {}

  async createUser(createUserDto: CreateUserDto): Promise<User> {
    const { password, ...userData } = createUserDto;

    const hashPassword = await hash(
      password,
      Number(process.env.SALT_ROUNDS) || 10,
    );

    const { password: resultPassword, ...resultUser } =
      await this.userRepository.save({
        ...userData,
        password: hashPassword,
      });

    return resultUser;
  }

  async findUser(id: number): Promise<User> {
    const user = await this.userRepository.findOneBy({ id });
    if (!user) throw new NotFoundException('User not found');

    return user;
  }

  async findUserByIdentifier(identifier: string): Promise<User> {
    const user = await this.userRepository.findOne({
      where: [{ username: identifier }, { email: identifier }],
      select: ['id', 'username', 'email', 'password'],
    });

    return user;
  }

  async findUsers(): Promise<User[]> {
    return this.userRepository.find();
  }

  async updateUser(id: number, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.findUser(id);

    return this.userRepository.save({
      ...user,
      ...updateUserDto,
    });
  }

  async deleteUser(id: number): Promise<void> {
    const result = await this.userRepository.delete(id);

    if (!result.affected) throw new NotFoundException('User not found');
  }
}
