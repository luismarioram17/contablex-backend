import { Injectable, NotFoundException } from '@nestjs/common';
import { AccountRepository } from 'src/data-source/repositories';
import { CreateAccountDto, UpdateAccountDto } from '../dtos';
import { Account } from 'src/data-source/entities';
import { InjectRepository } from '@nestjs/typeorm';
import { AccountType, types } from 'src/config/enums';
import { PaginationDto } from 'src/data-source/dtos/pagination.dto';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: AccountRepository,
  ) {}

  async createAccount(createAccountDto: CreateAccountDto): Promise<Account> {
    return this.accountRepository.save(createAccountDto);
  }

  async findAccounts(
    type?: AccountType,
    paginationDto?: PaginationDto,
  ): Promise<Account[]> {
    return this.accountRepository.find({
      where: type ? { type } : {},
      order: {
        label: 'ASC',
      },
      skip: paginationDto?.offset,
      take: paginationDto?.limit,
    });
  }

  async findAccount(id: number): Promise<Account> {
    const account = await this.accountRepository.findOneBy({ id });
    if (!account) throw new NotFoundException('Account not found');

    return account;
  }

  async accountTypeDropdown() {
    return types;
  }

  async updateAccount(
    id: number,
    updateAccountDto: UpdateAccountDto,
  ): Promise<Account> {
    const account = await this.findAccount(id);

    return this.accountRepository.save({
      ...account,
      ...updateAccountDto,
    });
  }

  async deleteAccount(id: number): Promise<void> {
    const result = await this.accountRepository.delete(id);

    if (!result.affected) throw new NotFoundException('User not found');
  }
}
