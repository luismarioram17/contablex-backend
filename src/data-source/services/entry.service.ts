import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Account, Entry, Transaction } from 'src/data-source/entities';
import {
  AccountRepository,
  EntryRepository,
  TransactionRepository,
} from 'src/data-source/repositories';
import { CreateEntryDto } from '../dtos';
import { PaginationDto } from '../dtos/pagination.dto';

@Injectable()
export class EntryService {
  constructor(
    @InjectRepository(Entry) private readonly entryRepository: EntryRepository,
    @InjectRepository(Transaction)
    private readonly transactionRepository: TransactionRepository,
    @InjectRepository(Account)
    private readonly accountRepository: AccountRepository,
  ) {}

  async createEntry(createEntryDto: CreateEntryDto) {
    const entry = await this.entryRepository.save(
      this.entryRepository.create({ ...createEntryDto, transactions: [] }),
    );

    const transactions = await Promise.all(
      createEntryDto.transactions.map(async (transaction) => {
        const { accountId, ...transactionData } = transaction;

        const account = await this.accountRepository.findOneBy({
          id: accountId,
        });

        return this.transactionRepository.create({
          ...transactionData,
          account,
          entry,
        });
      }),
    );

    await this.transactionRepository.save(transactions);

    return { ...entry, transactions };
  }

  async getEntries(pagination?: PaginationDto) {
    return this.entryRepository.find({
      relations: ['transactions', 'transactions.account'],
      skip: pagination?.offset,
      take: pagination?.limit,
    });
  }

  async getEntry(id: number) {
    const entry = await this.entryRepository.findOne({
      where: { id },
      relations: ['transactions', 'transactions.account'],
    });

    if (!entry) throw new NotFoundException('Entry not found');

    return entry;
  }

  async updateEntry(id: number, updateEntryDto: CreateEntryDto) {
    const entry = await this.entryRepository.findOneBy({ id });

    if (!entry) throw new NotFoundException('Entry not found');

    return this.entryRepository.save({
      ...entry,
      ...updateEntryDto,
    });
  }

  async deleteEntry(id: number) {
    const result = await this.entryRepository.delete(id);
    if (!result.affected) throw new NotFoundException('Entry not found');
  }
}
