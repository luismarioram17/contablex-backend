import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Account } from './account.entity';
import { Entry } from './entry.entity';

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne<Entry>('Entry', (entry) => entry.transactions, {
    onDelete: 'CASCADE',
  })
  entry?: Entry;

  @Column({
    type: 'decimal',
    transformer: {
      from: (value) => Math.floor(value * 100),
      to: (value) => value / 100,
    },
    default: 0,
  })
  ammountIn: number;

  @Column({
    type: 'decimal',
    transformer: {
      from: (value) => Math.floor(value * 100),
      to: (value) => value / 100,
    },
    default: 0,
  })
  ammountOut: number;

  @ManyToOne<Account>('Account', () => (account) => account.transactions)
  account: Account;
}
