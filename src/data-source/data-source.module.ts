import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { transformObjectToArray } from 'src/config/transform-object-to-array.helper';

import * as controllers from './controllers';
import * as entities from './entities';
import * as repositories from './repositories';
import * as services from './services';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ...transformObjectToArray(entities),
      ...transformObjectToArray(repositories),
    ]),
  ],
  controllers: [...transformObjectToArray(controllers)],
  providers: [...transformObjectToArray(services)],
  exports: [...transformObjectToArray(services)],
})
export class DataSourceModule {}
