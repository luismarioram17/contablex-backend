import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { UseJwtAuthGuard } from 'src/auth/decorators';
import { CreateEntryDto } from '../dtos';
import { EntryService } from '../services';

@Controller('entries')
@UseJwtAuthGuard()
export class EntryController {
  constructor(private readonly entryService: EntryService) {}

  @Get('/')
  getEntries(@Query('limit') limit?: number, @Query('offset') offset?: number) {
    return this.entryService.getEntries({
      limit,
      offset,
    });
  }

  @Get('/:id')
  getEntry(@Param('id') id: number) {
    return this.entryService.getEntry(id);
  }

  @Post('/')
  createEntry(@Body() createEntryDto: CreateEntryDto) {
    return this.entryService.createEntry(createEntryDto);
  }

  @Patch('/:id')
  updateEntry(@Param('id') id: number, @Body() updateEntryDto: CreateEntryDto) {
    return this.entryService.updateEntry(id, updateEntryDto);
  }

  @Delete('/:id')
  deleteEntry(@Param('id') id: number) {
    return this.entryService.deleteEntry(id);
  }
}
