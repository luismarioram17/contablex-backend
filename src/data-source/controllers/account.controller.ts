import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';

import { UseJwtAuthGuard } from 'src/auth/decorators';
import { AccountService } from 'src/data-source/services';
import { CreateAccountDto, UpdateAccountDto } from 'src/data-source/dtos';
import { AccountType } from 'src/config/enums';

@UseJwtAuthGuard()
@Controller('accounts')
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Post('/')
  createAccount(@Body() createAccountDto: CreateAccountDto) {
    return this.accountService.createAccount(createAccountDto);
  }

  @Get('/')
  findAccounts(
    @Query('type') type?: AccountType,
    @Query('limit') limit?: number,
    @Query('offset') offset?: number,
  ) {
    return this.accountService.findAccounts(type, { limit, offset });
  }

  @Get('/types')
  accountTypeDropdown() {
    return this.accountService.accountTypeDropdown();
  }

  @Get('/:id')
  findAccount(@Param('id') id: number) {
    return this.accountService.findAccount(id);
  }

  @Patch('/:id')
  updateAccount(
    @Param('id') id: number,
    @Body() updateAccountDto: UpdateAccountDto,
  ) {
    return this.accountService.updateAccount(id, updateAccountDto);
  }

  @Delete('/:id')
  deleteAccount(@Param('id') id: number) {
    return this.accountService.deleteAccount(id);
  }
}
