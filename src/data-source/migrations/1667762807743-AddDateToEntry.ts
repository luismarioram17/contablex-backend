import { MigrationInterface, QueryRunner } from "typeorm";

export class AddDateToEntry1667762807743 implements MigrationInterface {
    name = 'AddDateToEntry1667762807743'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entry" ADD "date" TIMESTAMP NOT NULL DEFAULT now()`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "entry" DROP COLUMN "date"`);
    }

}
