import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

import { Account } from 'src/data-source/entities';

@Injectable()
export class AccountRepository extends Repository<Account> {}
