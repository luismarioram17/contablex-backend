import { DataSource, Repository } from 'typeorm';
import { User } from 'src/data-source/entities';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserRepository extends Repository<User> {}
