import { Repository } from 'typeorm';
import { Entry } from 'src/data-source/entities';
import { Injectable } from '@nestjs/common';

@Injectable()
export class EntryRepository extends Repository<Entry> {}
