import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Transaction } from 'src/data-source/entities';

@Injectable()
export class TransactionRepository extends Repository<Transaction> {}
