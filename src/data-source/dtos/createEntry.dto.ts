import { Transform } from 'class-transformer';
import { IsDate, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import * as dayjs from 'dayjs';

import { CreateTransactionDto } from './createTransaction.dto';

export class CreateEntryDto {
  @IsString()
  @IsNotEmpty()
  description: string;

  @Transform(({ value }) => dayjs(value).toDate())
  @IsDate()
  date: string;

  @ValidateNested({ each: true })
  transactions: CreateTransactionDto[];
}
