import { OmitType, PartialType } from '@nestjs/mapped-types';
import { CreateEntryDto } from './createEntry.dto';

export class UpdateEntryDto extends PartialType(
  OmitType(CreateEntryDto, ['transactions']),
) {}
