import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateTransactionDto {
  @IsNumber()
  @IsOptional()
  ammountIn: number;

  @IsNumber()
  @IsOptional()
  ammountOut: number;

  @IsNumber()
  @IsNotEmpty()
  accountId: number;
}
