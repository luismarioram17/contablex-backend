import { Transform } from 'class-transformer';
import {
  IsBoolean,
  IsEnum,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

import { AccountType } from 'src/config/enums';

export class CreateAccountDto {
  @IsString()
  @MinLength(3)
  @MaxLength(3)
  @Transform(({ value }) => value.toUpperCase())
  @IsNotEmpty()
  label: string;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  description: string;

  @IsEnum(AccountType)
  type: AccountType;

  @IsBoolean()
  current: boolean;
}
