import { Controller, Post, UseGuards } from '@nestjs/common';
import { Request } from '@nestjs/common';

import { UseLocalAuthGuard } from 'src/auth/decorators';
import { AuthService } from 'src/auth/services';

@Controller()
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseLocalAuthGuard()
  @Post('auth/login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }
}
