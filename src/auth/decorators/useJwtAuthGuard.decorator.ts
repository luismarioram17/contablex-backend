import { UseGuards } from '@nestjs/common';

import { JwtAuthGuard } from 'src/auth/guards';

export function UseJwtAuthGuard() {
  return UseGuards(JwtAuthGuard);
}
