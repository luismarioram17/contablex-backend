import { UseGuards } from '@nestjs/common';

import { LocalAuthGuard } from 'src/auth/guards';

export function UseLocalAuthGuard() {
  return UseGuards(LocalAuthGuard);
}
